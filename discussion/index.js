// Packages 

const express = require('express');

// Mongoose is a package that allows creation of Schemas to model our data structures
// Also has access to a number of methods for manipulating our database.
const mongoose = require('mongoose');

// Configuration
const app = express();
const port = 5001;


app.use(express.json());
app.use(express.urlencoded({extended:true}));




// [SECTION] MongoDB Connection
// Connect to the database by passing in your connection string, remember to place the password and database names with actual values

// Connection to MongoDB Atlas

mongoose.connect("mongodb+srv://jazz456:<password>@cluster0.brhlyjl.mongodb.net/happy?retryWrites=true&w=majority", 
	{
		// In simple words,  "useNewUrlParser : true," allows us to avoid any current and future errors while connecting to MongoDB
		
		useNewUrlParser : true,

		// False by default. Set to true opt in to using MongoDB driver's new connection management engine. You should set this option to true, except for the unlikely case that it prevents you from maintaining a stable connection.

		useUnifiedTopology: true
	}

);


// Set notification for connection success or failure in MongoDB

let db = mongoose.connection;

// console.error.bind(console, "comments") allows ut to print errors in the browser console and in the terminal.
db.on('error', console.error.bind(console, "connection error"));

db.on('open', () => console.log("Connected to MongoDB!"));


// [SECTION] Mongoose Schemes

// Schemes act as blueprints to our date
// The 'new' keyword creates a new Schema

const taskSchema = new mongoose.Schema({ 
	// Define the fields with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	// There is a field called "name" and its data type is "String"
	name : String,
	// There is a field called "status" that is a "String" and the default value is "pending"
	status : { 
		type : String,
		// Default values are the predefined values for a field if we don't put any value
		default : "pending"
	}
});


// [SECTION] Models
// Uses schemas and are to create/instatiate objects that correspond to the schema
// Models use Schema and they act as the middleman from the server (JS CODE) to our database

// the variable/object 'Task' can nowuse to run commands for interacting with our database

// "Task" is capitalized following MVC(<pde. View and Control) approach for naming conventions.
// Models must be in singular form and capitalized

// For the first parameter of the Mongoose Model method indicates the collection in where to store the data

// for Second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection

// using Mongoose, the package was programmed well enough that is automatically coverts the singular form of the model name into plural form when creating a collection in postman.

const Task = mongoose.model('Task', taskSchema);



app.post('/tasks', (req, res) =>{
	// Check if there are duplicate tasks
	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches the search criteria
	// If there are no matches, the value of result is null
	// "err" is a shorthand naming convention for errors
	Task.findOne({name : req.body.name}, (err, result) => {

		// If a document was found and the document's name matches the information sent via the client/Postman
		if(result != null && result.name == req.body.name){

			// Return a message to the client/Postman
			return res.send("Duplicate task found");

		// If no document was found
		} else {

			// Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			});

			// The "save" method will store the information to the database
			// Since the "newTask" was created/instantiated from the Mongoose Schema it will gain access to this method to save to the database
			// The "save" method will accept a callback function which stores any errors found in the first parameter
			// The second parameter of the callback function will store the newly saved document
			// Call back functions in mongoose methods are programmed this way to store any errors in the first parameter and the returned results in the second parameter
			newTask.save((saveErr, savedTask) => {

				// If there are errors in saving
				if(saveErr){

					// Will print any errors found in the console
					// saveErr is an error object that will contain details about the error
					// Errors normally come as an object data type
					return console.error(saveErr);

				// No error found while creating the document
				} else {

					// Return a status code of 201 for created
					// Sends a message "New task created" on successful creation
					return res.status(201).send("New task created");

				}
			})
		}

	})
});



// Getting all the tasks

// Business Logic
/*
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks", (req, res) => {

	// "find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}, (err, result) => {

		// If an error occurred
		if (err) {

			// Will print any errors found in the console
			return console.log(err);

		// If no errors are found
		} else {

			// Status "200" means that everything is "OK" in terms of processing
			// The "json" method allows to send a JSON format for the response
			// The returned response is purposefully returned as an object with the "data" property to mirror real world complex data structures
			return res.status(200).json({
				data : result			
			})

		}

	})
})






app.listen(port, () => console.log(`Server running at port ${port}!`));





