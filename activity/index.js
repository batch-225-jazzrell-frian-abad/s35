const express = require('express');
const mongoose = require('mongoose');

// Configuration
const app = express();
const port = 5001;


app.use(express.json());
app.use(express.urlencoded({extended:true}));



mongoose.connect("mongodb+srv://jazz456:<password>@cluster0.brhlyjl.mongodb.net/Activity?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}

);


let db = mongoose.connection;

db.on('error', console.error.bind(console, "connection error"));

db.on('open', () => console.log("Connected to MongoDB!"));


const taskSchema = new mongoose.Schema({ 
	name : String,
	password : String,
});


const Task = mongoose.model('Task', taskSchema);



app.post('/signup', (req, res) =>{
	Task.findOne({name : req.body.name}, (err, result) => {

		// If a document was found and the document's name matches the information sent via the client/Postman
		if(result != null && result.name == req.body.name){

			// Return a message to the client/Postman
			return res.send("Duplicate user found");

		// If no document was found
		} else {

			// Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name,
				password : req.body.password
			});

			newTask.save((saveErr, savedTask) => {

				// If there are errors in saving
				if(saveErr){

					// Will print any errors found in the console
					// saveErr is an error object that will contain details about the error
					// Errors normally come as an object data type
					return console.error(saveErr);

				// No error found while creating the document
				} else {

					// Return a status code of 201 for created
					// Sends a message "New task created" on successful creation
					return res.status(201).send("New user registered");

				}
			})
		}

	})
});


app.get("/signup", (req, res) => {

	// "find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}, (err, result) => {

		// If an error occurred
		if (err) {

			// Will print any errors found in the console
			return console.log(err);

		// If no errors are found
		} else {

			// Status "200" means that everything is "OK" in terms of processing
			// The "json" method allows to send a JSON format for the response
			// The returned response is purposefully returned as an object with the "data" property to mirror real world complex data structures
			return res.status(200).json({
				Users : result			
			})

		}

	})
})








app.listen(port, () => console.log(`Server running at port ${port}!`));





